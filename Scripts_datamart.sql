/*==============================================================*/
/* Table: Empleado                                               */
/*==============================================================*/

drop TABLE IF EXISTS dim_Empleado;

create table dim_Empleado
(
   Empleado_id          int not null,
   Empleado_cedula      numeric,
   Empleado_nombre      text ,
   Empleado_apellido    text,
   primary key (Empleado_id)
);



/*==============================================================*/
/* Table: dim_tiempo                                        */
/*==============================================================*/

drop TABLE IF EXISTS dim_tiempo;

create table dim_tiempo
(
   dim_tiempo_id          int not null,
   año       							smallint,
	 Costo_arriendo       numeric,
   primary key (dim_tiempo_id)
);

/*==============================================================*/
/* Table: HEC_mejor_Vendedor                                        */
/*==============================================================*/

drop TABLE IF EXISTS HEC_mejor_Vendedor;

create table HEC_mejor_Vendedor
(
   Empleado_id         INT not null,
   dim_tiempo_id      int not null,
   HECHO                smallint,
   primary key (Empleado_id, dim_tiempo_id)
);

alter table HEC_mejor_Vendedor add constraint FK_DIMEMPLEADO_HEC_mejor_Vendedor foreign key (Empleado_id)
      references dim_Empleado (Empleado_id) on delete restrict on update restrict;

alter table HEC_mejor_Vendedor add constraint FK_DIMTIEMPO_HEC_mejor_Vendedor foreign key (dim_tiempo_id)
      references dim_tiempo (dim_tiempo_id) on delete restrict on update restrict;
			


